# 仓名

* [简介](#section1)
* [目录]()
* [相关仓]()

# 简介<a name="section1"></a>

为SIG仓提供文档、会议纪要及其他文档提供仓库，同时作为各SIG组的索引页

# 目录

样例：

```
├── ai_framework_integration  # ai集成框架sig目录
│   ├── docs                  # 其他资料目录
│   └── meetings              # 会议资料目录
├── architecture              #sig-architecture目录
│   ├── docs                  # 其他资料目录
│   └── meetings              # 会议资料目录
├── basicsoftwareservice      #sig-basicsoftwareservice目录
│   ├── docs                  # 其他资料目录
│   └── meetings              # 会议资料目录
├── blockchain                #sig-blockchain目录
│   ├── docs                  # 其他资料目录
│   └── meetings              # 会议资料目录
├── devboard                  #sig-devboard目录
│   ├── docs                  # 其他资料目录
│   └── meetings              # 会议资料目录
├── dllite_micro#sig-dllite_micro目录
│   ├── docs                  # 其他资料目录
│   └── meetings              # 会议资料目录
├── driver                    #sig-driver目录
│   ├── docs                  # 其他资料目录
│   └── meetings              # 会议资料目录
├── edu_data_specification#sig-目录
│   ├── docs                  # 其他资料目录
│   ├── figures
│   └── meetings              # 会议资料目录
├── industrial_internet       # sig-目录
│   ├── docs                  # 其他资料目录
│   └── meetings              # 会议资料目录
├── Infrastructure            #sig-industrial_internet目录
│   ├── docs                  # 其他资料目录
│   └── meetings              # 会议资料目录
├── knowlege                  # sig-knowledge目录
│   ├── docs                  # 其他资料目录
│   └── meetings              # 会议资料目录
├── linkboy                   #sig-linkboy 目录
│   ├── docs                  # 其他资料目录
│   └── meetings              # 会议资料目录
├── miniblink                 #sig-miniblink目录
│   ├── docs                  # 其他资料目录
│   └── meetings              # 会议资料目录
├── openblock                 #sig-openblock目录
│   ├── docs                  # 其他资料目录
│   └── meetings              # 会议资料目录
├── python                    #sig-python目录
│   ├── docs                  # 其他资料目录
│   └── meetings              # 会议资料目录
├── README.en.md              # 英文版仓描述文档
├── README.md                 # 中文版仓描述文档
├── release_management
│   └── docs
├── riscv                     #sig-riscv目录
│   ├── docs                  # 其他资料目录
│   ├── meetings              # 会议资料目录
│   └── task
└── sig_rules                 # sig门禁规则目录
    └── sig_rules.md          # sig门禁规则文档

```

# 说明 

注： 可添加仓使用说明，包括编译构建指导

# 相关仓

* [仓1]()    注：需要标清在sig下的相关的仓地址链接
* [仓2]()

