# July 15, 2022 at 14：00pm GMT+8

## Agenda
- 智慧城市蜂窝终端管理SIG启动会

## Attendees
- 彭昭
- 张海名
- 林毅
- 王芸
- 廖剑锋
- 谷龙龙
- 罗珊珊
- 张彬
- 万斌（九联科技）
- 袁友荣（移芯科技）
- 郭月飞（武汉天喻）
- 王友峰（利尔达）
- 聂世龙（吾爱易达）

## Notes
### **会议议程：**

1.主题介绍：OpenHarmony和智慧城市蜂窝终端管理SIG介绍

​				张彬对当前OpenHarmony发展情况、智慧城市蜂窝终端管理SIG的工作目标，工作范围，工作计划，共建内容做了简单介绍。

2.成员发言讨论：SIG共建成员确认，智慧城市蜂窝终端管理SIG运行机制和工作建议讨论



### **会议结论：**

1.与会成员确认加入智慧城市蜂窝终端管理SIG共建任务，并按照社区流程完成相关人员注册gitee账号，签署DCO协议等工作；

2.共同确定定期例会为每个月一次，时间为每月第一个星期二上午，如有特殊事项可以启动临时会议讨论，张彬负责发出会议通知。

3.按计划实施SIG申请内容，各成员伙伴在后续例会或临时会议中可以共同讨论扩充完善SIG工作内容。




## Action items
- 暂无