# 合规SIG例会 2022-09-02 15:30-16:30(UTC+08:00)Beijing

## 议题Agenda
- 合规SIG欧洲Oniro合规信息分享讨论 李怡爽
- Oniro合规扫描问题推动讨论 李怡爽
- 遗留问题及其他事项进展状态同步 陈雅旬/高亮

## 与会人Attendees
- 陈雅旬、郑志鹏、王意明、王亮、Swann(Oniro)、余甜、高亮 等

## 纪要Notes
- 会议纪要见

https://zulip.openharmony.cn/#narrow/stream/62-compliance_sig/topic/Meeting20220902

