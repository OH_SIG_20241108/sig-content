#  March 9, 2022 at 16:00am-18:00am GMT+8

## Agenda

- OH Driver SIG例会[](https://meeting.tencent.com/s/O6GmTrcZcV8a)

  |    时间     | 议题                                 | 发言人                                             |
  | :---------: | ------------------------------------ | ------------------------------------------------- |
  | 16:00-16:10 | 议题一、电源管理子系统battery模块新增HDI接口评审 | 胡军，张春鑫，翟海鹏，袁博，刘飞虎 |
  | 16:10-16:30 | 议题二、Codec新增2.0 HDI接口评审遗留问题 | 孙赫赫，张国荣，翟海鹏，袁博，刘飞虎 |
## Attendees

- @crescenthe(https://gitee.com/crescenthe)
- @vb6174(https://gitee.com/vb6174) 
- @zhangguorong(zhangguorong050@chinasoftinc.com)
- @YUJIA(https://gitee.com/JasonYuJia)
- @yuanbo(https://gitee.com/yuanbogit)
- @Kevin-Lau(https://gitee.com/Kevin-Lau)

  

## Notes

-   **议题一、电源管理子系统battery模块新增HDI接口评审**

    议题结论：新增battery模块HDI接口定义，接口和结构体设计合理，接口评审通过。 

    新增IDL接口如下： 

    GetTotalEnergy([out] int totalEnergy); 

    GetCurrentAverage([out] int curAverage); 

    GetCurrentNow([out] int curNow); 

    GetRemainEnergy([out] int remainEnergy); 

    GetBatteryInfo([out] struct CallbackInfo info); 

    遗留问题：1.CallbackInfo结构体命名不规范 2.idl注释缺少，需补充 。 

- **议题二、Codec新增2.0 HDI接口评审遗留问题**

    议题结论：新增codec 2.0版本HDI接口定义，遗留问题已闭环，接口评审通过。 

   

   遗留问题： 

   1. 结构体和函数参数优化 

      结构体已按照接口规范，优化完成接口定义：https://gitee.com/openharmony/drivers_peripheral/pulls/737/files

   2. Codec HDI2.0接口使用了OMX的部分结构体和枚举，之前是在头文件中引用OMX头文件，现在idl文件不支持引用外部头文件，需要把用到的结构体和枚举写在idl文件中。       

      孙赫赫已和OH法务部余甜确认，使用这些结构体和枚举是为了对接OMX，属于合理使用，法务合规。 

   

   会议通知：https://lists.openatom.io/hyperkitty/list/sig_driver@openharmony.io/thread/Z7JNZCNJJOZLCXGMSRCAOS7IR2FDKDSV/

   会议议题申报：https://etherpad.openharmony.cn/p/SIG-Driver

   会议归档：https://gitee.com/openharmony-sig/sig-content/tree/master/driver/meetings


  ## Action items
