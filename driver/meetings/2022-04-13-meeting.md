#  April 27, 2022 at 16:00am-18:00am GMT+8

## Agenda

- OH Driver SIG例会[](https://meeting.tencent.com/s/O6GmTrcZcV8a)

  |    时间     | 议题                                 | 发言人                                             |
  | :---------: | ------------------------------------ | ------------------------------------------------- |
  | 16:00-16:10 | 议题一、Media的Codec驱动能力开发进展 | 深开鸿，张国荣 |
  | 16:10-16:30 | 议题二、USB模块变更HDI接口评审 | 赵文华，翟海鹏，袁博，刘洪刚，刘飞虎，吴成文 |
  | 16:30-17:00 | 议题三、内核MemoryTracker模块新增HDI接口评审 | 赵文华，翟海鹏，袁博，刘洪刚，揣振中，王孝远，刘飞虎 |
## Attendees

- @crescenthe(https://gitee.com/crescenthe)

- @vb6174(https://gitee.com/vb6174) 

- @张国荣(zhangguorong050@chinasoftinc.com)

- @YUJIA(https://gitee.com/JasonYuJia)

- @袁博(https://gitee.com/yuanbogit)

- @刘飞虎(https://gitee.com/Kevin-Lau)

- @刘洪刚(https://gitee.com/liuhonggang123)

  

## Notes

-    **议题一、Media的Codec驱动能力开发进展---深开鸿 张国荣** 

    议题进展：Codec 1.0的接口已完成demo开发，编解码流程已打通，分批提交PR合入，计划4.30号完成上库。Codec HDI 2.0 BufferHandle功能已经实现，对应的demo已测试通过，下步启动与上层服务联调，计划待定。  

-  **议题二、USB模块变更HDI接口评审---赵文华，翟海鹏，袁博，刘洪刚，刘飞虎，吴成文** 

    议题结论：USB模块已定义的HDI接口做IDL转换，依据OH IDL规范，需要对接口进行拆分为读接口和写接口，经评审，兼容方案和接口拆解设计合理，接口评审通过。 

​       HDI接口列表： 

| **变更前接口**                                               | **接口功能**                      |
| ------------------------------------------------------------ | --------------------------------- |
| int32_t ControlTransfer(const UsbDev &dev, const UsbCtrlTransfer &ctrl, std::vector<uint8_t> &data); | Usb数据控制传输接口，支持读写数据 |
| 变更后接口                                                   |                                   |
| int32_t ControlTransferRead(const UsbDev &dev, const UsbCtrlTransfer &ctrl, std::vector<uint8_t> &data); | Usb数据控制传输接口，只支持读数据 |
| int32_t ControlTransferWrite(const UsbDev &dev, const UsbCtrlTransfer &ctrl, std::vector<uint8_t> &data); | Usb数据控制传输接口，只支持写数据 |

-   **议题三、内核MemoryTracker模块新增HDI接口评审—赵文华，翟海鹏，袁博，刘洪刚，揣振中，王孝远，刘飞虎**  

  议题结论：会议对新增MemoryTracker模块的目录，接口和数据接口进行评审，新增1个HDI接口，接口定义符合合规，兼容性和扩展性要求，接口评审通过。

  HDI接口和数据结构定义详见PR：

  https://gitee.com/openharmony/drivers_interface/pulls/54/files

  遗留问题：1、需要确认drivers/interface仓内核是否要单独规划目录？每个模块是否单独部署部件？  ----王孝远 

  

   会议通知：https://lists.openatom.io/hyperkitty/list/sig_driver@openharmony.io/thread/Z7JNZCNJJOZLCXGMSRCAOS7IR2FDKDSV/

   会议议题申报：https://etherpad.openharmony.cn/p/SIG-Driver

   会议归档：https://gitee.com/openharmony-sig/sig-content/tree/master/driver/meetings


  ## Action items
