#   November 2, 2022 at 16:00am-17:30am GMT+8

## Agenda

| 时间        | 议题                                   | 发言人 |
| ----------- | -------------------------------------- | ------ |
| 16:00-16:10 | 议题一、Media的codec驱动能力开发进展。 | 张国荣 |
| 16:10-16:30 | 议题二、南向HDI接口API资料讨论。       | 段夕超 |

## Notes

- **议题一、Media的codec驱动能力开发进展----张国荣，翟海鹏，刘飞虎，孙赫赫**

  议题进展：  Codec HDI2.0 IDL化已完成IDL分支合入，进行集成测试验收，验证通过合入主干。1.0接口解决直通模式demo问题。 

-  **议题二、南向HDI接口API资料讨论----刘飞虎，袁博，李艳，刘洪刚，黄一宏，段夕超** 

   议题结论：

  1、HDI外设驱动模块开发指南中，按照工具生成.h中的函数进行体现，并在开发指南中说明.h定义来自idl接口声明。 

   2、HDI接口参考文档中，每个API要体现支持的不同系统类型，在4.0版本中规划完成。 

 会议通知：(https://lists.openatom.io/hyperkitty/list/sig_driver@openharmony.io/thread/45GGTUFDSKBORQFGGCWFKU2JCXFZ5PBJ/)

 会议议题申报：(https://shimo.im/sheets/36GKhpvrXd8TcQHY/AdiEd) 

 Driver_sig邮件列表订阅:（https://lists.openatom.io/postorius/lists/sig_driver.openharmony.io/ ）

（操作方法<https://gitee.com/openharmony/drivers_framework/issues/I45X7P?from=project-issue>） 