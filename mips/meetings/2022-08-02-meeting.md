# August.2, 2022 at 10:00am GMT+8

## Agenda
|时间|议题|发言人|
|--|--|--|
|10:00-10:15|Mips编译工具链和编译框架适配进展同步 |袁祥仁 黄慧进 刘佳科|
|10:15-10:25|系统三方组件适配进展同步 |Lain 袁祥仁 刘佳科|
|10:25-10:35|子系统适配进展同步 |袁祥仁 刘佳科|
|10:35-10:40|XTS子系统适配问题沟通|郑曦 刘佳科|

## Attendees
- [@hongtao6573](https://gitee.com/hongtao6573)
- [@cip_syq](yunqiang.su@oss.cipunited.com)
- [@Lain]()
- [@wicom](https://gitee.com/wicom)
- [@wangxing-hw](https://gitee.com/wangxing-hw)
- [@libing23](https://gitee.com/libing23)
- [@huanghuijin](https://gitee.com/huanghuijin)
- [@huangsox](https://gitee.com/huangsox)
- [@liujk000](https://gitee.com/liujk000)


## Notes

#### 议题一、Mips编译工具链和编译框架适配进展同步

**结论**
- 针对mips架构的llvm+clang编译工具链已经可以编译完成，黄慧进提议可以提交pr审核。 责任人：刘佳科
- 编译框架适配任务预计下周前跑通编译流程。 责任人：袁祥仁 Lain


#### 议题二、系统三方组件适配进展同步

**结论**
- Lain查找了mips的msa技术资料，目前认为flutter相关的三方库适配难度较大，计划本周提交issue到对应仓库，请社区予以指导和帮助。ssl相关的三方库还需要分析，下次例会前

#### 议题三、子系统适配进展同步

**结论**
- 1.君正已经完成histreamer的mips平台相关适配，8月16日前提交相关改动。

#### 议题四、新大陆XTS子系统适配问题沟通

**问题1**
- 在3.1release版本进行适配过兼容性认证，是否会有超出支持时效的风险？
- 社区专家确认无风险。

**问题2**
- 新大陆使用nfs挂载方式运行xts测试套件，出现挂载的共享目录下无法获得可执行权限导致测试失败的问题，需要帮助。
- 联系xts子系统专家帮忙分析。责任人：刘佳科

## Action items
