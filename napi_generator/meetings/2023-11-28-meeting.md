## Agenda

- OH 辅助工具 SIG沟通会

  |    时间     | 议题                       | 发言人       |
  | :---------: | -------------------------- | ------------ |
  | 17:00-18:00 | napi_generator美团问题情况对齐 | 龚俊松，刘鑫，赵军霞，苏重威 |

## Attendees

- [@zhaojunxia2020](https://gitee.com/zhaojunxia2020)
- [xliu-huanwei](https://gitee.com/xliu-huanwei)
- 龚俊松
- wangjingwu
- lixingyang
- chenshulin
- 沈春萍

## Notes

- 会议内容：

  1. 对齐napi_generator工具使用问题
  2. 对齐27号会议遗留问题修改进展
  3. 对齐native线程抛js任务实现方案

  ## Action items

  1、使用2023.11发布的新版本测试  -- 刘鑫

  2、统一文档中命令行工具下载链接，更新到新版本 -- 赵军霞

  3、研究native线程抛js任务实现方案的可行性与方案 -- 赵军霞
  
  